<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Shop</title>
        <link href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="col-xs-3 list-group">
                <a class="list-group-item filter" data-filter="all" href="#">All</a>
                <a class="list-group-item filter" data-filter="Horse" href="#">Horse</a>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <?php
                    foreach ($products as $product) {
                        ?>
                        <div class="col-xs-4 <?php echo $product->name ?> product">
                            <div class="thumbnail">
                                <?php
                                echo img(
                                        array(
                                            'src' => 'assets/img/' . $product->image,
                                            'class' => 'img-responsive',
                                            'alt' => $product->name,
                                        )
                                );
                                ?>
                                <div class="caption">
                                    <h3><?php echo $product->name; ?></h3>
                                    <div class="price">$ <?php echo $product->price ?></div>
                                    <div class="option">
                                        <?php if ($product->option_name) { ?>
                                            <?php echo form_label($product->option_name, 'option_' . $product->id); ?>
                                            <?php
                                            echo form_dropdown($product->option_name, $product->option_values, NULL, 'id="option_' . $product->id . '"');
                                            ?>
                                        <?php } ?>
                                    </div>
                                    <button class="item btn btn-primary" data-item="<?php echo $product->id ?>">Add to Cart</button>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-xs-6">
                <div id="cart">
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() . 'assets/js/jquery-1.12.3.min.js'; ?>"></script>
        <script src="<?php echo base_url() . 'assets/js/bootstrap.min.js'; ?>"></script>
        <script>
            $(document).ready(function () {
                $('#cart').load('<?php echo base_url() . 'shop/loadCart' ?>');
                $('.item').click(function (e) {
                    e.preventDefault();
                    var id = $(this).data('item');
                    var selected_option = "option_" + id;
                    var option = $('#' + selected_option).val();
                    $('#cart').fadeOut();
                    $.post("<?php echo base_url() . 'shop/add' ?>",
                            {
                                id: id,
                                option_name: option
                            }, function (data) {
                        $('#cart').html(data);
                        $('#cart').fadeIn();
                    });
                });
                
                $(".filter").click(function () {
                    var value = $(this).attr('data-filter');
                    $('.filter').removeClass('active');
                    
                    if (value == "all"){
                        $('.product').show('1000');
                        $(this).addClass('active');
                    } else{
                        $(".product").not('.' + value).hide('3000');
                        $('.product').filter('.' + value).show('3000');
                        $(this).addClass('active');
                    }
                });
            });
        </script>
    </body>
</html>