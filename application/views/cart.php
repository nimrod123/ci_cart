<?php if ($cart = $this->cart->contents()) { ?>
    <table class="table">
        <caption>Shopping Cart</caption>
        <tr>
            <th>Item Name</th>
            <th>Quantity</th>
            <th>Option</th>
            <th>Price</th>
            <th>Subtotal</th>
            <th></th>
        </tr>
        <?php foreach ($cart as $item) { ?>
            <tr>
                <td><?php echo $item['name']; ?></td>
                <td><?php echo $item['qty']; ?></td>
                <td>
                    <?php
                    if ($this->cart->has_options($item['rowid'])) {
                        foreach ($this->cart->product_options($item['rowid']) as $option => $value) {
                            echo $option . ": <em>" . $value . '</em>';
                        }
                    }
                    ?>
                </td>
                <td><?php echo '&#8369; ' . number_format($item['price'], 2, '.', ','); ?></td>
                <td><?php echo '&#8369; ' . number_format($item['subtotal'], 2, '.', ','); ?></td>
                <td class="remove">
                    <a href="#" data-row ="<?php echo $item['rowid'] ?>" class="delete_cart btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">Total: </td>
            <td><?php echo '&#8369; ' . number_format($this->cart->total(), 2, '.', ','); ?></td>
        </tr>
    </table>

    <?php
} else {
    echo "CART IS EMPTY";
}
?>

<?php
if ($this->cart->contents()) {
    ?>
    <center><button class="order btn btn-primary">Place Order</button>
        <button class="reset btn btn-primary">Reset</button>
    </center>
    <?php
}
?>

<script>
    $('.delete_cart').click(function (e) {
        e.preventDefault();
        var row = $(this).data('row');
        $('#cart').fadeOut();
        $.post("<?php echo base_url() . 'shop/remove' ?>",
                {
                    row: row
                }, function (data) {
            $('#cart').html(data);
        });
        setTimeout(function () {
            $('#cart').fadeIn();
        }, 800);
    });
    $('.order').click(function (e) {
        e.preventDefault();
        $.post("<?php echo base_url() . 'shop/placeOrder' ?>", {},
                function (data) {
                    $('#cart').html(data);
                });
        setTimeout(function () {
            $('#cart').fadeIn();
        }, 800);
    });
    $('.reset').click(function (e) {
        e.preventDefault();
        $('#cart').fadeOut();
        $.post("<?php echo base_url() . 'shop/emptyCart' ?>", {},
                function (data) {
                    $('#cart').html(data);

                });
        setTimeout(function () {
            $('#cart').fadeIn();
        }, 800);
    });
</script>


