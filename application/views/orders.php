<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Shop</title>
        <link href="<?php echo base_url() . 'assets/css/bootstrap.min.css'; ?>" rel="stylesheet">
    </head>
    <body>
        <div class="btn btn-primary">New Orders <i class="badge" id="notif"></i></div>
        <div class="container">
            <div id="orders">
            </div>
        </div>
        <script src="<?php echo base_url() . 'assets/js/jquery-1.12.3.min.js'; ?>"></script>
        <script src="<?php echo base_url() . 'assets/js/bootstrap.min.js'; ?>"></script>
        <script>
            $("#orders").load("<?php echo base_url() . 'order/printOrders'; ?>");

            setInterval(function () {
                $.post("<?php echo base_url() . 'order/latestOrder' ?>", {}, function (data) {
                    if(data!= 0){
                        $('#notif').text(data);
                        $("#orders").load("<?php echo base_url() . 'order/printOrders'; ?>");
                    }
                });
            }, 1000);
        </script>
    </body>
</html>