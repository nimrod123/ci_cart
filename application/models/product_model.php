<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function get_all() {
        $results = $this->db->get('products')->result();
        foreach ($results as $result) {
            if ($result->option_values) {
                $result->option_values = explode(',', $result->option_values);
            }
        }
        return $results;
    }

    public function get($id) {
        $results = $this->db->get_where('products', array('id' => $id))->result();

        $result = $results[0];
        if ($result->option_values) {
            $result->option_values = explode(',', $result->option_values);
        }
        return $result;
    }

    public function addOrder($id, $qty, $option) {
        $data = array(
            'product_id' => $id,
            'qty' => $qty,
            'option' => $option,
            'status' => 'pending'
        );
        $this->db->insert('orders',$data);
        return true;
    }

}
