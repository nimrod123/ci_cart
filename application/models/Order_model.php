<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    function get() {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('products', 'orders.product_id = products.id');
        $this->db->order_by('date','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getLatestOrder() {
        $this->db->select_max('date');
        $query = $this->db->get('orders');
        $row = $query->row_array();
        return $row['date'];
    }

    function countLatestOrder($date) {
        $this->db->where('date >', $date);
        $count = $this->db->get('orders')->num_rows();
        return $count;
    }

}
