<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('image_lib');
        $this->load->helper(array('captcha'));
        $this->load->model('product_model');
    }

    function index() {
        $data['products'] = $this->product_model->get_all();

        $this->load->view('products', $data);
    }

    function add() {
        $product = $this->product_model->get($this->input->post('id'));
        $insert = array(
            'id' => $this->input->post('id'),
            'qty' => 1,
            'price' => $product->price,
            'name' => $product->name
        );
        if ($product->option_name) {
            $insert['options'] = array(
                $product->option_name => $product->option_values[$this->input->post('option_name')]
            );
        }
        $this->cart->insert($insert);
        $this->loadCart();
    }

    function remove() {
        $row = $this->input->post('row');
        $data = array(
            'rowid' => $row,
            'qty' => 0
        );
        $this->cart->update($data);
        $this->loadCart();
    }

    function loadCart() {
        $this->load->view('cart');
    }

    function emptyCart() {
        $this->cart->destroy();
        $this->loadCart();
    }

    function placeOrder() {
        if ($cart = $this->cart->contents()) {
            
            foreach ($cart as $item) {
                $id = $item['id'];
                $qty = $item['qty'];
                $selected='';
                if ($this->cart->has_options($item['rowid'])) {
                    foreach ($this->cart->product_options($item['rowid']) as $option => $value) {
                        $selected = $option.': '.$value;
                    }
                }
                $this->product_model->addOrder($id, $qty, $selected);
            }
            $this->cart->destroy();
        }
        echo "nakapasok na sa DB";
    }
    
    function captcha(){
        $data = array(
            'img_path' => './static/',
            'img_url' => base_url().'static',
            'img_width' => '150',
            'img_height' => '40',
            'font_size' => 30
        );
        
        $captcha = create_captcha($data);
        $this->load->view('captchademo', $captcha);
    }

}
