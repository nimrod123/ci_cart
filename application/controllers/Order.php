<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('order_model');
    }

    function index() {
        $this->load->view('orders');
    }

    function printOrders() {
        $latest = $this->order_model->getLatestOrder();
        $this->session->set_userdata('order_time', $latest);
        $data['orders'] = $this->order_model->get();
        $this->load->view('order_table', $data);
    }

    function latestOrder() {
        $present = $this->session->userdata('order_time');
        $new_orders = $this->order_model->countLatestOrder($present);
        if ($new_orders > 0) {
            $latest = $this->order_model->getLatestOrder();
            $this->session->set_userdata('order_time', $latest);
        }
        echo $new_orders;
    }

}
