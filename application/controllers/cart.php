<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    function add() {
        $data = array(
        'id' => '1',
        'name' => 'Pants',
        'qty' => 1,
        'price' => 19.99,
        'options' => array('Size' => 'medium')
        );
        $this->cart->insert($data);
    }
    
    function show(){
        print_r($this->cart->contents());
    }
    
    function update(){
        $data = array(
            'rowid'=>'4f0dd88ab5efed35dbd65b4d2ac4dc27',
            'qty'=>0
        );
        $this->cart->update($data);
    }
    
    function total(){
        echo $this->cart->total();
    }
    
    function destroy(){
        $this->cart->destroy();
    }

}
